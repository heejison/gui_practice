# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\pc\PycharmProjects\Kiosk__\UI\Update_UI.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.

import mariadb

from PyQt5 import QtCore, QtGui, QtWidgets

def db_connect():
    conn = mariadb.connect(host = '127.0.0.1', user = 'root', password = '1014', db = 'kiosk')
    return conn

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.comboBox = QtWidgets.QComboBox(Dialog)
        self.comboBox.setGeometry(QtCore.QRect(40, 160, 71, 41))
        font = QtGui.QFont()
        font.setFamily("나눔고딕 ExtraBold")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.comboBox.setFont(font)
        self.comboBox.setCurrentText("")
        self.comboBox.setObjectName("comboBox")
        self.id_line = QtWidgets.QLineEdit(Dialog)
        self.id_line.setGeometry(QtCore.QRect(130, 70, 161, 41))
        font = QtGui.QFont()
        font.setFamily("나눔고딕")
        font.setPointSize(14)
        self.id_line.setFont(font)
        self.id_line.setObjectName("id_line")
        self.update = QtWidgets.QPushButton(Dialog)
        self.update.setGeometry(QtCore.QRect(300, 160, 71, 41))
        font = QtGui.QFont()
        font.setFamily("나눔고딕 ExtraBold")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.update.setFont(font)
        self.update.setObjectName("update")
        self.combo_line = QtWidgets.QLineEdit(Dialog)
        self.combo_line.setGeometry(QtCore.QRect(130, 160, 161, 41))
        font = QtGui.QFont()
        font.setFamily("나눔고딕")
        font.setPointSize(14)
        self.combo_line.setFont(font)
        self.combo_line.setObjectName("combo_line")
        self.OK = QtWidgets.QPushButton(Dialog)
        self.OK.setGeometry(QtCore.QRect(300, 70, 71, 41))
        font = QtGui.QFont()
        font.setFamily("나눔고딕 ExtraBold")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.OK.setFont(font)
        self.OK.setObjectName("OK")
        self.ID = QtWidgets.QLabel(Dialog)
        self.ID.setGeometry(QtCore.QRect(50, 70, 61, 41))
        font = QtGui.QFont()
        font.setFamily("나눔고딕 ExtraBold")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.ID.setFont(font)
        self.ID.setObjectName("ID")
        self.back = QtWidgets.QPushButton(Dialog)
        self.back.setGeometry(QtCore.QRect(10, 10, 75, 23))
        font = QtGui.QFont()
        font.setFamily("나눔고딕 ExtraBold")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.back.setFont(font)
        self.back.setObjectName("back")
       # self.OK.clicked.connect(lambda: self.OK_clicked(Dialog))
        self.update.clicked.connect(lambda: self.update_clicked(Dialog))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    """ def OK_clicked(self, Dialog):
        conn = db_connect()
        cur = conn.cursor()

        sql = "insert into user values ('{}','{}','{}')".format(self.id_line.text(), self.pw_line.text(),
                                                                self.name_line.text())
        cur.execute(sql)

        conn.commit()

        msg = QtWidgets.QMessageBox()

        msg.information(Dialog, "알림", "확인되었습니다.")"""

    def update_clicked(self, Dialog):
        conn = db_connect()
        cur = conn.cursor()

        # sql 문 오류
        # sql = "UPDATE user SET PASSWORD = ('{}') WHERE ID = ('{}')".format(self.pw_line.text(), self.id_line.text())
        sql = "UPDATE user SET PASSWORD = '" + self.pw_line.text() + "' WHERE id = '" + self.id_line.text() + "'"
        cur.execute(sql)

        conn.commit()

        print(sql)
        print(self.pw_line.text())
        print(self.id_line.text())

        msg = QtWidgets.QMessageBox()

        msg.information(Dialog, "알림", "수정되었습니다.")

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.update.setText(_translate("Dialog", "수정"))
        self.OK.setText(_translate("Dialog", "입력"))
        self.ID.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:14pt;\">ID</span></p></body></html>"))
        self.back.setText(_translate("Dialog", "뒤로가기"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
